package inheritance;

public class Book {
	protected String title;
	private String author;

	public Book(String title, String author) {
		this.title = title;
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public String toString() {
		return "Title: " + getTitle() + ", Author: " + getAuthor();
	}

//	public static void main(String[] args) {
//		ElectronicBook ebook = new ElectronicBook("Inheritance", "Neil Fisher", 10);
//		System.out.println(ebook.toString());
//	}
}