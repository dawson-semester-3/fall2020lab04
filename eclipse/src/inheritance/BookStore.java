package inheritance;

public class BookStore {
	public static void main(String[] args) {
		Book[] books = new Book[5];
		books[0] = new Book("Book1", "Author1");
		books[1] = new ElectronicBook("eBook2", "Author2", 2);
		books[2] = new Book("Book3", "Author3");
		books[3] = new ElectronicBook("eBook4", "Author4", 4);
		books[4] = new ElectronicBook("eBook5", "Author5", 5);

		for (Book book : books) {
			System.out.println(book.toString());
		}
	}
}
