package geometry;

public class LotsOfShapes {
	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		shapes[0] = new Rectangle(2, 3);
		shapes[1] = new Rectangle(4, 5);
		shapes[2] = new Circle(2);
		shapes[3] = new Circle(3);
		shapes[4] = new Square(5);

		for (Shape shape : shapes) {
			System.out.println("Shape: " + shape.getClass().getSimpleName());
			System.out.println("Area: " + shape.getArea());
			System.out.println("Perimeter: " + shape.getPerimeter());
			System.out.println();
		}
	}
}