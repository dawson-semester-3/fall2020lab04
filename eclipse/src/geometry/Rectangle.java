package geometry;

public class Rectangle implements Shape {
	private double length, width;

	public Rectangle(double length, double width) {
		this.length = length;
		this.width = width;
	}

	public double getLength() {
		return length;
	}

	public double getWidth() {
		return width;
	}

	public double getArea() {
		return getLength() * getWidth();
	}

	public double getPerimeter() {
		return 2 * getLength() + 2 * getWidth();
	}
}
